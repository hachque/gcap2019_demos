# GCAP 2019 Demos - "So you want to make a multiplayer game?"

These examples demonstrate the concepts outlined in the ["So you want to make a multiplayer game?" talk](https://docs.google.com/presentation/d/1geqthI-xxciyRKqKSj6wfZRjdXF_g-QfUlgj37QDygM/edit?usp=sharing).

## Presentation Links

I had more links than I could put on one closing slide, so here's all of them.

* [Source Multiplayer Networking](https://developer.valvesoftware.com/wiki/Source_Multiplayer_Networking)
* [Protogame - Input Prediction Implementation](https://github.com/RedpointGames/Protogame/blob/master/Protogame/Network/LowLevel/InputPrediction.cs)
* [Protogame - Interpolation Implementation](https://github.com/RedpointGames/Protogame/tree/master/Protogame/Network/TimeMachines)
* [Armello Blog Post on Dedicated Servers](https://steamcommunity.com/games/290340/announcements/detail/1696062196980619562)
* [Combining deterministic lockstep and input prediction - Factorio](https://www.factorio.com/blog/post/fff-302)
